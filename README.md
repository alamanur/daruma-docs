### Documentation for Daruma Websiste

This documentation currently descruibes visual/physical design of the website.
![](asset/dashboard.png)
The commomn feature for each page is,
- header top area ( social menu, company info)
- site logo/branding 
- menu 
- footer 
- footer bottom


### Pages
* [Home](home.md)
* [About Us](about-us.md)
* Companies
    * [Daruma Consulting Ltd.](page.md)
    * [Daruma Bulders & Engineering Ltd.](page.md)
    * [Daruma Trading Corporation](page.md)
* [Team](team.md)
* [Services](services.md)
* [Contact](contact.md)






