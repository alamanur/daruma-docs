### Homepage

![](asset/home-page.png)
1. social menu/links
2. contact info
3. site branding 
    * logo
    * name (Daruma Goup)
    * title ( Consulting, Builders & Engineering, Tradeing)
4. Main menu
    * [Home](home.md)
    * [About Us](about-us.md)
    * Companies
        * [Daruma Consulting Ltd.](page.md)
        * [Daruma Bulders & Engineering Ltd.](page.md)
        * [Daruma Trading Corporation](page.md)
    * [Team](team.md)
    * [Services](services.md)
    * [Contact](contact.md)
5. Featured Sliders
6. Key Features ( Daruma Group )
7. Daruma Consulting Ltd.
    * logo 
    * header (Daruma Consulting Ltd.)
    * short description 
    * [link to page ( Know More)](Daruma Consulting Ltd.md ) 
8. Daruma Bulders & Engineering Ltd.
    * logo 
    * header (Daruma Bulders & Engineering Ltd.)
    * short description 
    * [link to page ( Know More)](Daruma Consulting Ltd.md ) 
9. Daruma Trading Corporation
    * logo 
    * header (Daruma Trading Corporation)
    * short description 
    * [link to page ( Know More)](Daruma Consulting Ltd.md ) 
10. Link to [Contact](contact.md) page
11. Ongoing Projects ( three projects )
    * Project image
    * Project name
    * Project short detail
12. Tema (  top 4 members) 
    * team member avatar 
    * team member name 
    * team member designation 
    * team member social links
13. Footer section
Address info for three companies.
14. Footer Bottom
    * copyright text 
    * developer information 